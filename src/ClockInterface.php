<?php

declare(strict_types=1);

namespace Psr\Clock;

/** @see https://github.com/php-fig/fig-standards/blob/master/proposed/clock.md#21-clockinterface */
interface ClockInterface
{
    /**
     * Returns the current time as a DateTimeImmutable Object
     */
    public function now(): \DateTimeImmutable;
}
