<?php

declare(strict_types=1);

namespace Psr\Clock\Tests;

use DateTimeImmutable;
use PHPUnit\Framework\TestCase;
use Psr\Clock\FrozenClock;

class FrozenClockTest extends TestCase
{
    public function testSameAsNewDateTimeIfTimezoneIsNotUTC(): void
    {
        // Given
        date_default_timezone_set('America/Vancouver');
        $frozenClock = new FrozenClock('2021-09-20 12:12:12');

        // Then
        self::assertEquals(new DateTimeImmutable('2021-09-20 12:12:12'), $frozenClock->now());
    }

    public function testNotSameAsNewIfTimezoneChanges(): void
    {
        // Given
        date_default_timezone_set('America/Vancouver');
        $frozenClock = new FrozenClock('2021-09-20 12:12:12');

        // When
        date_default_timezone_set('Pacific/Tahiti');

        // Then
        self::assertNotEquals(new DateTimeImmutable('2021-09-20 12:12:12'), $frozenClock->now());
    }
}
